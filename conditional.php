<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih Function PHP</h1> 
    <h2>ARFIANSYAH</h2>

    <?php
    echo "<h3> Soal No 1 Greetings </h3>";
    function greetings($nama){
        echo "Halo" . $nama . ", Selamat Datang di PKS Digital School!<br>";
    }
        greetings("Bagas");
        greetings("Wahyu");
        greetings("Abdul");
    
        echo "<br>";

        echo "<h3>Soal No 2 Reverse String</h3>";
        function reverse($kata1){
            $panjangkata = strlen($kata1);
            $tampung = "";
            for($r=($panjangkata - 1); $r>=0; $r--){
                $tampung .= $kata1[$r];
            }
            return $tampung;
        }
        function reverseString($kata2){
            $string=reverse($kata2);
            echo $string ."<br>";
        }
            reverseString("abduh");
            reverseString("Digital School");
            reverseString("We Are PKS Digital School Developers");
            echo "<br>";

        echo "<h3>Soal No 3 Palindrome </h3>";
        function palindrome($kata3){
            $balik = reverse ($kata3);
            if ($kata3 == $balik){
                 echo "true <br>";
            }else {
                 echo "false <br>";
            } 
        }      
        palindrome("civic") ; // true
        palindrome("nababan") ; // true
        palindrome("jambaban"); // false
        palindrome("racecar"); // true
        
        echo "<h3>Soal No 4 Tentukan Nilai </h3>";  
        function Tentukan_Nilai($angka){
            $output = "";
        if($angka>=85 && $angka<100){
            $output .= "Sangat Baik";
        }else if($angka>=70 && $angka<85){
            $output .= "Baik";
        }else if($angka>=60 && $angka<70){
            $output .= "Cukup";   
        }else if($angka<70){
            $output .= "Kurang";
        }
        return $output ."<br>";
    }
    echo tentukan_nilai(98); //Sangat Baik
    echo tentukan_nilai(76); //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang          
    ?>
</body>
</html>